const chai = require('chai');
const chaiHttp = require('chai-http');
const app = require('../app');

const expect = chai.expect;

chai.use(chaiHttp);

describe('CRUD Operations', () => {
  let itemId;

  before((done) => {
    // Create a test item before running the tests
    chai.request(app)
      .post('/items')
      .send({ id: 1, name: 'Test Item' })
      .end((err, res) => {
        itemId = res.body.id;
        done();
      });
  });

  it('should create a new item', (done) => {
    chai.request(app)
      .post('/items')
      .send({ id: 2, name: 'New Item' })
      .end((err, res) => {
        expect(res).to.have.status(201);
        expect(res.body).to.have.property('id', 2);
        done();
      });
  });

  it('should get all items', (done) => {
    chai.request(app)
      .get('/items')
      .end((err, res) => {
        expect(res).to.have.status(200);
        expect(res.body).to.be.an('array');
        done();
      });
  });
});
