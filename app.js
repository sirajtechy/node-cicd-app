const express = require('express');
const bodyParser = require('body-parser');

const app = express();

app.use(bodyParser.json());

const items = [];

// Create
app.post('/items', (req, res) => {
  const newItem = req.body;
  items.push(newItem);
  res.status(201).json(newItem);
});

// Read
app.get('/items', (req, res) => {
  res.json(items);
});

// Update
app.put('/items/:id', (req, res) => {
  const itemId = req.params.id;
  const updatedItem = req.body;

  const index = items.findIndex(item => item.id === itemId);
  if (index !== -1) {
    items[index] = updatedItem;
    res.json(updatedItem);
  } else {
    res.status(404).json({ message: 'Item not found' });
  }
});

// Delete
app.delete('/items/:id', (req, res) => {
  const itemId = req.params.id;

  const index = items.findIndex(item => item.id === itemId);
  if (index !== -1) {
    items.splice(index, 1);
    res.json({ message: 'Item deleted' });
  } else {
    res.status(404).json({ message: 'Item not found' });
  }
});

module.exports = app;
